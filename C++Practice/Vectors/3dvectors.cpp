#include<iostream>
using namespace std;

class Vectors{
	public:
	int x;
	int y;
	int z;
	friend istream& operator >>(istream& is,Vectors& obj){
		cout<<"Enter the x component:";
		is>>obj.x;
		cout<<"Enter the y component:";
		is>>obj.y;
		cout<<"Enter the z component:";
		is>>obj.z;
		return is;
	}
	friend ostream& operator <<(ostream& os,Vectors& obj){
		cout<<"("<<obj.x<<")i(+"<<obj.y<<")j+("<<obj.z<<")k"<<endl;
		return os;
    }
 	friend Vectors operator +(Vectors a, Vectors b){
		Vectors result1;
		result1.x=a.x+b.x;
		result1.y=a.y+b.y;
 		result1.z=a.z+b.z;
		return result1;
	}
	friend Vectors operator *(Vectors a, Vectors b){
		Vectors result2;
		result2.x=a.x*b.x;
		result2.y=a.y*b.y;
		result2.z=a.z*b.z;
		cout<<"Dot Product is:"<<result2.x+result2.y+result2.z<<endl;
		return result2;
	}
	friend Vectors operator ^(Vectors a, Vectors b){
		Vectors result3;
		
		result3.x=a.y*b.z;
		result3.y=a.x*b.z;
		result3.z=a.x*b.y;
		return result3;
	}
	
};
int main(){
	Vectors obj1,obj2;
	cin>>obj1;
	cin>>obj2;
	cout<<"Vector1 is:"<<obj1;
	cout<<"Vector2 is:"<<obj2;
	Vectors obj3;
	obj3=obj1+obj2;
	cout<<"Resultant:"<<obj3;
	Vectors obj4;
	obj4=obj1*obj2;
	Vectors obj5;
	obj5=obj1^obj2;
	cout<<"Cross Product:"<<obj5;
	return 0;
}	


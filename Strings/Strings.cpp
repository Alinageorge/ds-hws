#include"Strings.h"

Strings::Strings(){
	LB=0;
	UB=0;
}
Strings::Strings(int u){
	LB=0;
	UB=u;
	int i;
	cout<<"Enter the characters of strings one by one:"<<endl;
	for (i=LB;i<UB;i++){
		cin>>S[i];
	}
}
int Strings::length() const{
	int i=-1;
	while(*(S+(++i))!='\0');
	return i;
}
int Strings::compare(const Strings& s){
	int i=0;
	while (S[i]==s.S[i] && S[i]!='\0' && s.S[i]!='\0'){
		i++;	
	}
	return S[i]-s.S[i];	
}
char* Strings::string_concat(const Strings& s){
	int l1=length();
	int l2=s.length();
	char* S3=new char[l1+l2+1];
	int i;
	for (i=0;i<l1;i++){
		S3[i]=S[i];
	}
	for (i=0;i<l2;i++){
		S3[l1+i]=s.S[i];
	}
	S3[l1 + l2] = '\0';
	return S3;
}
char* Strings::substring(int start,int len){
	char* T=new char[len+1];
	int i;
	for(i=0;i<len;i++){
		T[i]=S[start-1+i];
	}
	T[len+1]='\0';
	return T;
}
void Strings::substringMatch(const Strings& s){
	int pos=0;
	int l1=length();
	int l2=s.length();
	while (pos<=l1-l2){
		int j=0;
		while(S[pos+j]==s.S[j] && s.S[j]!='\0'){
			j=j+1;
		}
		if (s.S[j]=='\0'){
			cout<<"Substring found at"<<pos+1;
		}
		pos=pos+1;
	}
	cout<<"Substring not found"<<endl;		
}

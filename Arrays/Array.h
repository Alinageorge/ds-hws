#include<iostream>
#include<ostream>
using namespace std;
template <class T>
class Array{
		int LB,UB;
		T A[100];
	public:
		Array();
		Array(int,int,T[]);
		void insert_at_end(T);
		void insert_at_beg(T);
		void insert_at_index(T,int);
		void del_at_end();
		void del_at_beg();
		void del_at_index(int);
		T linear_search(T);
		void swap(int,int);
		void selection_sort();
		T binary_search(T);
		void bubble_sort();
		void insertion_sort();
		int partition(int,int);
		void quick_sort(int,int);
		void printDistinct();
		void freq();
		template<class U>
		friend ostream& operator <<(ostream&,Array<U>);	
};



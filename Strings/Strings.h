#include<iostream>
#include<ostream>
using namespace std;

class Strings{
		int LB,UB;
		char S[100];
	public:
		Strings();
		Strings(int);
		int length() const;
		int compare(const Strings&);
		char* string_concat(const Strings&);
		char* substring(int,int);
		void substringMatch(const Strings&);
};

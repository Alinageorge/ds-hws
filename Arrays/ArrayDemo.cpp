#include"Array.cpp"
int main(){
	Array<int> myArray;
	myArray.insert_at_end(3);
	myArray.insert_at_end(5);
	myArray.insert_at_end(9);
	myArray.insert_at_end(8);
	myArray.insert_at_end(2);
	myArray.insert_at_end(3);
	myArray.insert_at_beg(6);
	myArray.insert_at_beg(1);
	myArray.insert_at_index(1,3);
	myArray.insert_at_index(4,4);

/*	myArray.del_at_beg();*/	
	myArray.del_at_end();
	myArray.del_at_index(7);
	cout<<myArray;
	int ind1=myArray.linear_search(5);
	cout<<"Index in linear search:"<<ind1<<endl;
	
	myArray.selection_sort();
	int ind2=myArray.binary_search(6);
	cout<<"Index in binary search:"<<ind2<<endl;
	myArray.bubble_sort();
	myArray.insertion_sort();
	
	myArray.quick_sort(1,5);
	myArray.printDistinct();
	myArray.freq();
	cout<<myArray;
	
/*	Array<float> myfArray;
	myfArray.insert_at_end(3.5);
	myfArray.insert_at_end(5.5);
	myfArray.insert_at_beg(6.3);
	
	cout<<myfArray;*/
	
	
	return 0;

}

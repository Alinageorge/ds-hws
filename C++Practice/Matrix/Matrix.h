#include<iostream>
using namespace std;

class Matrix{
	public:
	int i,j,k,mat[3][3];
	friend istream& operator >>(istream& is,Matrix& m){
	   	cout<<"Enter the entries of  matrix"<<endl;
   		for (m.i=0;m.i<3;m.i++){
       		for(m.j=0;m.j<3;m.j++){
           		is>>m.mat[m.i][m.j];
            }
    	}
   		return is;
    }
    friend ostream& operator <<(ostream& os,Matrix& m){
    	for (m.i=0;m.i<3;m.i++){
       		for(m.j=0;m.j<3;m.j++){
           		os << m.mat[m.i][m.j] << " ";
            }
            os<<endl;
    	}
    	return os;
	} 
    friend Matrix operator +(Matrix a,Matrix b){
    	Matrix sum;
    	for (sum.i=0;sum.i<3;sum.i++){
    		for(sum.j=0;sum.j<3;sum.j++){
				sum.mat[sum.i][sum.j]=a.mat[sum.i][sum.j]+b.mat[sum.i][sum.j];
			}
		}
    	return sum;
	}
	friend Matrix operator -(Matrix a,Matrix b){
    	Matrix diff;
    	for (diff.i=0;diff.i<3;diff.i++){
    		for(diff.j=0;diff.j<3;diff.j++){
				diff.mat[diff.i][diff.j]=a.mat[diff.i][diff.j]-b.mat[diff.i][diff.j];
			}
		}
    	return diff;
	}
	friend Matrix operator *(Matrix a,Matrix b){
    	Matrix product;
    	for (product.i=0;product.i<3;product.i++){
    		for(product.j=0;product.j<3;product.j++){
    			product.mat[product.i][product.j]=0;
    			for(product.k=0;product.k<3;product.k++)
    			
				product.mat[product.i][product.j]+=a.mat[product.i][product.k]*b.mat[product.k][product.j];
			}
		}
    	return product;
	}
				
};

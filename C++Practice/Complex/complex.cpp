#include<iostream>
using namespace std;

class Complex{
	public:
	int real;
	int img;
	friend istream& operator >>(istream& is,Complex& obj){
		cout<<"Enter the real part:";
		is>>obj.real;
		cout<<"Enter the imaginary part:";
		is>>obj.img;
		return is;
	}
	friend ostream& operator <<(ostream& os,Complex& obj){
		if(obj.img>0){
			cout<<obj.real<<"+"<<obj.img<<"i"<<endl;
		}
		else{
			cout<<obj.real<<obj.img<<"i"<<endl;
		}
		
		return os;
    	}
 	friend Complex operator +(Complex a, Complex b ){
		Complex result1;
		result1.real=a.real+b.real;
		result1.img=a.img+b.img;
		return result1;
	}
	friend Complex operator *(Complex a, Complex b ){
		Complex result2;
		result2.real=(a.real*b.real)-(a.img*b.img);
		result2.img=(a.real*b.img)+(b.real*a.img);
		return result2;
	}
	void operator --(){
		img=-img;
		if(img>0){
			cout<<"Complement of the Complex Number is:"<<real<<"+"<<img<<"i"<<endl;
		}
		else{
			cout<<"Complement of the Complex Number is:"<<real<<img<<"i"<<endl;
		}
			
	}
};
int main(){
	Complex obj1,obj2;
	cin>>obj1;
	cin>>obj2;
	cout<<"Complex Number1 is:"<<obj1;
	cout<<"Complex Number2 is:"<<obj2;
	Complex obj3;
	obj3=obj1+obj2;
	cout<<"Sum:"<<obj3;
	Complex obj4;
	obj4=obj1*obj2;
	cout<<"Product:"<<obj4;
	--obj1;
	--obj2;
	return 0;
}	



#include"Array.h"

template<class T>
Array<T>::Array(){
	LB=1;
	UB=0;
}
template <class T>
Array<T>::Array(int LB, int UB,T x[]){
	
}
template<class T>
void Array<T>::insert_at_end(T key){

	UB=UB+1;
	A[UB]=key;
}
template<class T>
void Array<T>::insert_at_beg (T key){
	int k;	
	UB=UB+1;
	k=UB-1;
	while (k>=LB){
		A[k+1]=A[k];
		k=k-1;
	}	
	A[LB]=key;
}
template<class T>
void Array<T>::insert_at_index(T key,int pos){
	int k;
	UB=UB+1;
	k=UB-1;
	while (k>=pos){
		A[k+1]=A[k];
		k=k-1;
	}
	A[pos]=key;
}
template<class T>
void Array<T>::del_at_index(int pos){
	if (LB<=pos and pos<=UB){
		int k;
		k=pos+1;
		while (k<=UB){
			A[k-1]=A[k];
			k=k+1;
		}
		UB=UB-1;
	}
	else{
		cout<<"Out of Bound";
	}
}
template<class T>
void Array<T>::del_at_end(){
	UB=UB-1;
}
template<class T>
void Array<T>::del_at_beg(){
	UB=UB-1;
	int k;
	while (k>=LB){
		A[k-1]=A[k];
	}
}
template<class T>
T Array<T>::linear_search(T key){
	int index=-1;
	int i=LB;
	while(i<=UB){
		if(A[i]==key){
			index=i;
			break;
		}
		i=i+1;
	}
	return index;
}
template<class T>
void Array<T>::swap(int p,int q)
{
	T t=A[p];
	A[p]=A[q];
	A[q]=t;
}
template<class T>
void Array<T>::selection_sort()
{
	for(int i=LB;i<UB;i++)
	{
		int min=i;
		for(int j=i+1;j<=UB;j++)
		{
			if (A[j]<A[min])
			{
				min=j;
			}
		}
		if (min!=i)
		{
			swap(i,min);
		}
	}
}
template<class T>
T Array<T>::binary_search(T key)
{
	int p=LB;
	int q=UB;
	while (p<=q)
	{
		int mid=(p+q)/2;
		if (A[mid]>key)
		{
			q=mid-1;
		}
		else if(A[mid]<key)
		{
			p=mid+1;
		}
		else
		{
			return mid;
		}
	}
	return NULL;
}
template<class T>
void Array<T>::bubble_sort()
{
	for(int i=LB;i<UB;i++)
	{
		for(int j=LB;j<UB+LB-i;j++)
		{
			if (A[j]>A[j+1])
			{
				swap(j,j+1);
			}
		}
	}
}
template <class T>
void Array<T>::insertion_sort(){
	int i;
	i=LB+1;
	while(i<=UB){
		T key=A[i];
		int j;
		j=i-1;
		while(j>=LB and A[j]>key){
			A[j+1]=A[j];
			j=j-1;
		}
		A[j+1]=key;
		i=i+1;
	}
}
template<class T>
int Array<T>::partition(int LB,int UB){
	int j=LB-1;
	T pivot=A[UB];
	int i=LB;
	while (i<=UB-1)
	{
		if (A[i]<pivot)
		{
			j=j+1;
			swap(i,j);
		}
		i+=1;	
	}
	swap(j+1,UB);
	return j+1;
}
template <class T>
void Array<T>::quick_sort(int LB,int UB){
    if (LB < UB) {
        int p = partition(LB, UB);
        quick_sort(LB,p-1);
        quick_sort(p+1,UB);
    }
}	
template <class T>
void Array<T>::freq(){
	int i,j;
	cout<<"Frequency of elements are:"<<endl;
	for(i=0;i<UB;i++){
		int count=0;
		for(j=0;j<UB;j++){
			if(A[i]==A[j]){
				count=count+1;
			}
		}
		cout<<A[i]<<" : "<<count<<endl;
	}
}
template <class T>
void Array<T>::printDistinct(){
	cout<<"Printing distinct elements only:";
	int i,j;
	for(i=0;i<UB;i++){
		int count=0;					
		for(j=0;j<UB;j++){
			if(A[i]==A[j]){
				count=count+1;
			}
		}
		if(count==1){
			cout<<A[i]<<" ";
		}
	}
	cout<<endl;
}		
template <class U>
ostream& operator <<(ostream& os,Array<U> M){
	int i;
	os<<endl;
	for(i=M.LB;i<=M.UB;i++){
		os<<M.A[i]<<" ";
	}
	os<<endl;
	return os;
}
